#pragma once


/*
!!!!!  WARNING  !!!!!

All definitions here are hand-made and

NOT TESTED
*/

union __dr0_t {
	unsigned debug;
};


union __dr1_t {
	unsigned debug;
};


union __dr2_t {
	unsigned debug;
};


union __dr3_t {
	unsigned debug;
};

//dr4, dr5 reserved

union __dr6_t {
	unsigned debug;
	struct 
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;

		unsigned always_set : 8;
		unsigned always_unset : 1;

		unsigned bd : 1;
		unsigned bs : 1;
		unsigned bt : 1;
		unsigned rtm : 1;

		unsigned reserved0_always_set : 15;
	}bits;
};


union __dr7_t
{
	unsigned debug;
	struct 
	{
		unsigned l0 : 1;
		unsigned g0 : 1;
		unsigned l1 : 1;
		unsigned g1 : 1;
		unsigned l2 : 1;
		unsigned g2 : 1;
		unsigned l3 : 1;
		unsigned g3 : 1;
		unsigned le : 1;
		unsigned ge : 1;
		unsigned always_set_1 : 1;
		unsigned rtm : 1;
		unsigned always_unset_0 : 1;
		unsigned gd : 1;
		unsigned always_unser_1 : 2;
		unsigned r_w_0 : 2;
		unsigned len_0 : 2;
		unsigned r_w_1 : 2;
		unsigned len_1 : 2;
		unsigned r_w_2 : 2;
		unsigned len_2 : 2;
		unsigned r_w_3 : 2;
		unsigned len_3 : 2;

	}bits;
};