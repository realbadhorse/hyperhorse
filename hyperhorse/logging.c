#include <wdm.h>
#include <stdarg.h>

void log_debug_ok(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vDbgPrintExWithPrefix("[#] ", DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, fmt, args);
	va_end(args);
}

void log_debug_er(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vDbgPrintExWithPrefix("-!- ", DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, fmt, args);
	va_end(args);
}

