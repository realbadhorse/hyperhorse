#pragma once

typedef struct _SEGMENT_DESCRIPTOR
{
    union
    {
        __int64 As__int64;
        struct
        {
            __int16 LimitLow;        // [0:15]
            __int16 BaseLow;         // [16:31]
            __int32 BaseMiddle : 8;  // [32:39]
            __int32 Type : 4;        // [40:43]
            __int32 System : 1;      // [44]
            __int32 Dpl : 2;         // [45:46]
            __int32 Present : 1;     // [47]
            __int32 LimitHigh : 4;   // [48:51]
            __int32 Avl : 1;         // [52]
            __int32 LongMode : 1;    // [53]
            __int32 DefaultBit : 1;  // [54]
            __int32 Granularity : 1; // [55]
            __int32 BaseHigh : 8;    // [56:63]
        } Fields;
    };
} SEGMENT_DESCRIPTOR, * PSEGMENT_DESCRIPTOR;

typedef struct _SEGMENT_ATTRIBUTE
{
    union
    {
        __int16 As__int16;
        struct
        {
            __int16 Type : 4;        // [0:3]
            __int16 System : 1;      // [4]
            __int16 Dpl : 2;         // [5:6]
            __int16 Present : 1;     // [7]
            __int16 Avl : 1;         // [8]
            __int16 LongMode : 1;    // [9]
            __int16 DefaultBit : 1;  // [10]
            __int16 Granularity : 1; // [11]
            __int16 Reserved1 : 4;   // [12:15]
        } Fields;
    };
} SEGMENT_ATTRIBUTE, * PSEGMENT_ATTRIBUTE;
